import configparser
import os
from plugins import pm
builtin = True;
config = configparser.ConfigParser()
config.read("config.ini")
if not os.path.exists(".bgm.ogg"):
	print("Downloading background music...")
	pm.download("https://gitlab.com/TurboWafflz/turbowafflz.gitlab.io/-/raw/master/public/.bgm.ogg", ".bgm.ogg")
try:
	import pygame
	pygame.init()
	pygame.mixer.music.load(".bgm.ogg")
	pygame.mixer.music.play(loops=-1)
except:
	pass